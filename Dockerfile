FROM alpine:3.9
LABEL maintainer="David Hamm <david.hamm@hammwerk.de>"

# install build tools
RUN apk update
RUN apk add git make cmake g++ linux-headers libjpeg-turbo-dev imagemagick v4l-utils-dev

# checkout mjpg-streamer
RUN git clone https://github.com/jacksonliam/mjpg-streamer.git

WORKDIR mjpg-streamer/mjpg-streamer-experimental

# build mjpg-streamer
RUN make USE_LIBV4L2=true

ENTRYPOINT ./mjpg_streamer -i "./input_uvc.so -r ${RESOLUTION:-1920x1080}" -o "./output_http.so -w ./www"
