# What is mjpg-streamer?

[mjpg-streamer](https://sourceforge.net/projects/mjpg-streamer/) is a command line application that copies JPEG frames from one or more input plugins to multiple output plugins. It can be used to stream JPEG files over an IP-based network from a webcam to various types of viewers such as Chrome, Firefox, Cambozola, VLC, mplayer, and other software capable of receiving MJPG streams.

It was originally written for embedded devices with very limited resources in terms of RAM and CPU. Its predecessor "uvc_streamer" was created because Linux-UVC compatible cameras directly produce JPEG-data, allowing fast and perfomant M-JPEG streams even from an embedded device running OpenWRT. The input module "input_uvc.so" captures such JPG frames from a connected webcam. mjpg-streamer now supports a variety of different input devices.

# How to use this image

## Start a mjpg-streamer instance

```
docker run -d
           --name mjpg-streamer
           --privileged
           -v /dev/video0:/dev/video0
           -p 8080:8080
           -e RESOLUTION="1280x720"
           --restart always
       davidhamm/mjpg-streamer
```

- **`--privileged`**  
  is necessary vor mjpg-streamer to match Container intern user rights to Host user rights
- **`-v /dev/video0:/dev/video0`**  
  mounts a video device from host into this container
- **`-p 8080:8080`**  
  binds the host port '8080' to the coinainers internal port '8080'. mjpg-streamer is listening by default on port '8080' inside the container
- **`-e RESOLUTION="1280x720"`**  
  if present, mjpg-streamer will stream with this resolution. else it will stream with 1920x1080
- **`--restart always`**  
  restarts the container always as long as the user doesn't shut's it down

## Webinterface

mjpg-streamer includes a webinterface that is available under this URL:

```
http://localhost:8080
```

## Raw stream and snapshot URL's

To access the stream open this URL:

```
http://localhost:8080/?action=stream
```

To get a single JPEG just open this URL:

```
http://localhost:8080/?action=snapshot
```
